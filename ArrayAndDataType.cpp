
#include <iostream>
#include <numeric>
#include "Helpers.h"

using namespace std;

int main(int argc, char* argv[])
{
    const int sizeArray = 10;

    int today = getToday();

    int remainder = today % sizeArray;
    
    int new_array[sizeArray][sizeArray];

    cout << "Array:" << endl;
    for (int i = 0; i < sizeArray; i++)
    {
        for (int j = 0; j < sizeArray; j++)
        {
            new_array[i][j] = i + j;

            cout << new_array[i][j] << ends;
        }
        cout << endl;
    }
    cout << "remainder of the division:" << ends;
    cout << accumulate(begin(new_array[remainder]), end(new_array[remainder]), 0) << endl;
    
    return 0;
}
