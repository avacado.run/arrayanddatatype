﻿#include <iostream>

using namespace std;

int getToday()
{
    struct tm newtime;
    time_t now = time(0);
    localtime_s(&newtime,&now);
    return newtime.tm_mday;
}